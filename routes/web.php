<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\WebController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {return view('web.index');})->name('web.index');
Route::get('/', [WebController::class, 'index'])->name('web.index');
Route::get('/contact-us', function () {return view('web.contact-us');})->name('web.contact-us');
Route::get('/about-us', function () {return view('web.about-us');})->name('web.about-us');
Route::get('/menu-list', function () {return view('web.menu-list');})->name('web.menu-list');
Route::get('/menu-grid', function () {return view('web.menu-grid');})->name('web.menu-grid');
