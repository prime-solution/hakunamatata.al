<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index(){

        $menuItems = [];

        $menuItem = ['name'=>'Margarita', 'ingredients'=>'Salce , Moxarella', 'price_1'=>'350','price_2'=>'650'];
        $menuItem2 = ['name'=>'Proshute', 'ingredients'=>'Salce , Moxarella, Proshute', 'price_1'=>'400','price_2'=>'750'];
        $menuItem3 = ['name'=>'Sallam', 'ingredients'=>' Salce , Moxarella, Sallam', 'price_1'=>'400','price_2'=>'750'];
        $menuItem4 = ['name'=>'Proshute Sallam', 'ingredients'=>' Salce , Moxarella, Proshute, Sallam', 'price_1'=>'450','price_2'=>'800'];
        $menuItem5 = ['name'=>'Kerpudha', 'ingredients'=>' Salce , Moxarella, Kerpudha', 'price_1'=>'400','price_2'=>'750'];
        $menuItem6 = ['name'=>'Divola', 'ingredients'=>' Salce , Moxarella, Sallam Pikant', 'price_1'=>'400','price_2'=>'750'];
        $menuItem7 = ['name'=>'4 Djatherat', 'ingredients'=>' Salce , Moxarella, Gorgonxola, Kackavall , Parmixhano', 'price_1'=>'400','price_2'=>'750'];
        $menuItem8 = ['name'=>'4 Stinet', 'ingredients'=>' Salce , Moxarella, Proshute, Sallam, Ton , Kerpudhe', 'price_1'=>'450','price_2'=>'800'];
        $menuItem9 = ['name'=>'Ton', 'ingredients'=>' Salce , Moxarella,Ton , Ullinj', 'price_1'=>'450','price_2'=>'800'];
        $menuItem10 = ['name'=>'Proshute Kerpudhe', 'ingredients'=>' Salce , Moxarella, Proshute , Kerpudhe', 'price_1'=>'450','price_2'=>'800'];
        $menuItem11 = ['name'=>'Kapricoze', 'ingredients'=>' Salce , Moxarella, Proshute , Kerpudhe,Speca,Ullinj', 'price_1'=>'550','price_2'=>'900'];
        $menuItem12 = ['name'=>'Love', 'ingredients'=>' Salce , Moxarella, Sallam pikant , Gorgonzola', 'price_1'=>'550','price_2'=>'900'];
        $menuItem13 = ['name'=>'Hakuna-Matata', 'ingredients'=>' Salce , Moxarella, Vaj hudhre,Gorgonzola, Kerpudhe , Speca, Parmigiano', 'price_1'=>'700','price_2'=>'1050'];
        $menuItem14 = ['name'=>'Vegjetariane', 'ingredients'=>' Salce , Moxarella, Perime', 'price_1'=>'450','price_2'=>'700'];
        $menuItem15 = ['name'=>'La Crema', 'ingredients'=>' Salce , Moxarella, Sallam Pikant , Pana', 'price_1'=>'550','price_2'=>'900'];
        $menuItem16 = ['name'=>'Rukola', 'ingredients'=>' Salce , Moxarella, Rukola , Pomodorini,Grana', 'price_1'=>'550','price_2'=>'900'];
        $menuItem17 = ['name'=>'Wrustel', 'ingredients'=>' Salce , Moxarella, Wudy', 'price_1'=>'450','price_2'=>'800'];
        $menuItem18 = ['name'=>'Bianca', 'ingredients'=>'Moxarella, Pana, Djath i bardhe', 'price_1'=>'300','price_2'=>'700'];
        $menuItem19 = ['name'=>'3 Sallamet', 'ingredients'=>'Salce,Moxarella, Milanez, Pikant , Parizier', 'price_1'=>'450','price_2'=>'800'];
        $menuItem20 = ['name'=>'3 Desideri', 'ingredients'=>'Salce,Moxarella, Krudo, Rukola , Parmigiano, Pomodorini', 'price_1'=>'600','price_2'=>'950'];

        array_push($menuItems,$menuItem);
        array_push($menuItems,$menuItem2);
        array_push($menuItems,$menuItem3);
        array_push($menuItems,$menuItem4);
        array_push($menuItems,$menuItem5);
        array_push($menuItems,$menuItem6);
        array_push($menuItems,$menuItem7);
        array_push($menuItems,$menuItem8);
        array_push($menuItems,$menuItem9);
        array_push($menuItems,$menuItem10);
        array_push($menuItems,$menuItem11);
        array_push($menuItems,$menuItem12);
        array_push($menuItems,$menuItem13);
        array_push($menuItems,$menuItem14);
        array_push($menuItems,$menuItem15);
        array_push($menuItems,$menuItem16);
        array_push($menuItems,$menuItem17);
        array_push($menuItems,$menuItem18);
        array_push($menuItems,$menuItem19);
        array_push($menuItems,$menuItem20);

        $sandwiches = [];

        $sandwich1 = ['name'=>'Fshati','price'=>'150'];
        $sandwich2 = ['name'=>'Proshute','price'=>'180'];
        $sandwich3 = ['name'=>'Sallam','price'=>'180'];
        $sandwich4 = ['name'=>'4 djathrat','price'=>'180'];
        $sandwich5 = ['name'=>'Proshute Kerpudhe','price'=>'200'];
        $sandwich6 = ['name'=>'Pikant','price'=>'180'];
        $sandwich7 = ['name'=>'Love','price'=>'230'];
        $sandwich8 = ['name'=>'Mix','price'=>'230'];
        $sandwich9 = ['name'=>'Ton','price'=>'230'];

        array_push($sandwiches,$sandwich1);
        array_push($sandwiches,$sandwich2);
        array_push($sandwiches,$sandwich3);
        array_push($sandwiches,$sandwich4);
        array_push($sandwiches,$sandwich5);
        array_push($sandwiches,$sandwich6);
        array_push($sandwiches,$sandwich7);
        array_push($sandwiches,$sandwich8);
        array_push($sandwiches,$sandwich9);

        $kalcones = [];

        $kalcone1 = ['name'=>'Kapricoza','price'=>'300','ingredients'=>'Salce , Moxarella , Proshute , Kerpudhe , Speca , Ullinj'];
        $kalcone2 = ['name'=>'Proshute','price'=>'280','ingredients'=>'Salce , Moxarella , Proshute'];
        $kalcone3 = ['name'=>'Sallam','price'=>'280','ingredients'=>'Salce , Moxarella , Sallam'];
        $kalcone4 = ['name'=>'4 Djathrat','price'=>'300','ingredients'=>'Salce , Moxarella , Gorgonxola , Parmigiano'];
        $kalcone5 = ['name'=>'Proshute kerpudhe','price'=>'300','ingredients'=>'Salce , Moxarella , Proshute , Kerpudhe'];
        $kalcone6 = ['name'=>'Vegjetariane','price'=>'300','ingredients'=>'Salce , Moxarella , Perime'];
        $kalcone7 = ['name'=>'Wrustel','price'=>'300','ingredients'=>'Salce , Moxarella , Wudy'];

        array_push($kalcones,$kalcone1);
        array_push($kalcones,$kalcone2);
        array_push($kalcones,$kalcone3);
        array_push($kalcones,$kalcone4);
        array_push($kalcones,$kalcone5);
        array_push($kalcones,$kalcone6);
        array_push($kalcones,$kalcone7);

        $salads = [];

        $salad1 = ['name'=>'Sallate Jeshile','price'=>'200'];
        $salad2 = ['name'=>'Sallate Greke','price'=>'350'];
        $salad3 = ['name'=>'Sallate Rukola','price'=>'350'];
        $salad4 = ['name'=>'Sallate Hakuna Matata','price'=>'400'];
        $salad5 = ['name'=>'Sallate Mix','price'=>'350'];

        array_push($salads,$salad1);
        array_push($salads,$salad2);
        array_push($salads,$salad3);
        array_push($salads,$salad4);
        array_push($salads,$salad5);

        return view('web.index')->with([
            'menuItems'  => $menuItems,
            'sandwiches' => $sandwiches,
            'kalcones'   => $kalcones,
            'salads'     => $salads,
        ]);
    }
}
