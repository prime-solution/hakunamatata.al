@extends('web.layouts.app')

@section('content')

    <section class="banner">
        <div class="banner-carousel owl-carousel">

            <div class="banner-slide">
                <div class="container">
                    <div class="banner-box">
                        <div class="banner-text">
                            <div class="banner-center">
                                <h2 class="banner-headding">Ha<span>ku</span>na Ma<span>ta</span>ta</h2>
                                <p class="banner-sub-hed">Pica me e mire ne qytet</p>
                            </div>
                        </div>
                        <div class="banner-images">
                            <div class="all-img-banner">
                                <img src="{{asset('web-images/banner-bg-1.png')}}" alt="banner" class="pizza-img">
                                <img src="{{asset('images/banner-bg-2.png')}}" alt="banner" class="pizza-it pizza-1">
                                <img src="{{asset('images/banner-bg-3.png')}}" alt="banner" class="pizza-it pizza-2">
                                <img src="{{asset('images/banner-bg-4.png')}}" alt="banner" class="pizza-it pizza-3">
                                <img src="{{asset('images/banner-bg-5.png')}}" alt="banner" class="pizza-it pizza-4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-slide-2">
                <div class="container">
                    <div class="banner-box">
                        <div class="banner-text">
                            <div class="banner-center">
                                <h2 class="banner-headding">Cilesi A<span>bso</span>lute</h2>
                                <p class="banner-sub-hed">Shije e jashtezakonshme , tashme prane jush </p>
                            </div>
                        </div>
                        <div class="banner-images">
                            <div class="all-img-banner">
                                <img src="{{asset('web-images/pizza-banner-1.png')}}" alt="banner" class="pizza-img">
                                <img src="{{asset('images/pizza-1.png')}}" alt="banner" class="pizza-it pizza-1">
                                <img src="{{asset('images/pizza-2.png')}}" alt="banner" class="pizza-it pizza-2">
                                <img src="{{asset('images/pizza-3.png')}}" alt="banner" class="pizza-it pizza-3">
                                <img src="{{asset('images/pizza-4.png')}}" alt="banner" class="pizza-it pizza-4">
                                <img src="{{asset('images/pizza-5.png')}}" alt="banner" class="pizza-it pizza-5">
                                <img src="{{asset('images/pizza-6.png')}}" alt="banner" class="pizza-it pizza-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-slide-3">
                <div class="container">
                    <div class="banner-box">
                        <div class="banner-images">
                            <div class="all-img-banner">
                                <img src="{{asset('web-images/pizza-banner-2.png')}}" alt="banner" class="pizza-img">
                                <img src="{{asset('images/pizza-7.png')}}" alt="banner" class="pizza-it pizza-1">
                                <img src="{{asset('images/pizza-8.png')}}" alt="banner" class="pizza-it pizza-2">
                                <img src="{{asset('images/pizza-9.png')}}" alt="banner" class="pizza-it pizza-3">
                                <img src="{{asset('images/pizza-10.png')}}" alt="banner" class="pizza-it pizza-4">
                                <img src="{{asset('images/pizza-11.png')}}" alt="banner" class="pizza-it pizza-5">
                                <img src="{{asset('images/pizza-12.png')}}" alt="banner" class="pizza-it pizza-6">
                            </div>
                        </div>
                        <div class="banner-text">
                            <div class="banner-center">
                                <h2 class="banner-headding">Ushqim C<span>ile</span>sor</h2>
                                <p class="banner-sub-hed">Produktet me te fresketa ne pjaten tuaj!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="order-section ptb order-section-with-icons">
        <img src="{{asset('web-images/monkey3.png')}}" alt="banner" class="top-left-icon">
        <img src="{{asset('web-images/timon-pumbaa.png')}}"   alt="banner" class="bottom-right-icon">
        <div class="container">
            <div class="row">
                <div class="order-top"><img src="{{asset('images/order-top.png')}}" alt="layer"></div>
                <div class="col-xl-4 col-lg-4 col-md-4 servose-box text-center padding-lf">
                    <img src="{{asset('images/order-1.svg')}}" alt="order" class="order-img">
                    <h2 class="order-title text-uppercase">porosisni tani</h2>
                    <p class="order-des">Porosisni ushqimin tuaj nepermjet telefonit , rrjeteve sociale ose aplikacionit web.
                    Cilesia dhe shpejtesia jane te garantuara</p>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 servose-box text-center padding-lf">
                    <img src="{{asset('images/order-2.svg')}}" alt="delivery" class="order-img">
                    <h2 class="order-title text-uppercase">delivery ose pick up</h2>
                    <p class="order-des">Porosisni ushqimin tuaj dhe ne mundesojme sherbim delivery
                        ose mund te paraqiteni prane ambjenteve tona dhe ushqimi juaj do te jete gati.</p>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 servose-box text-center padding-lf">
                    <img src="{{asset('images/order-3.svg')}}" alt="delicious" class="order-img">
                    <h2 class="order-title text-uppercase">Recete e shijshme</h2>
                    <p class="order-des">Recetat tona jane unike dhe tejet te shijshme. Provoni ushqimin tone dhe do te mbeteni te kenaqur</p>
                </div>
                <div class="order-bottom"><img src="{{asset('images/order-bottom.png')}}" alt="layer"></div>
            </div>
        </div>
    </section>

    <section class="speciality ptb pt-140">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="headding-part text-center pb-50">
                        <p class="headding-sub">Me te mirat nga Hakuna-Matata</p>
                        <h2 class="headding-title text-uppercase font-weight-bold">Specialiteti yne</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 text-center speciality-box">
                    <div class="speciality-img"><a href="#"><img src="{{asset('web-images/speciality-1.jpg')}}" alt="speciality" class="spec-image"></a></div>
                    <a href="#" class="ser-title text-uppercase font-weight-bold">Hakuna Matata</a>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 text-center speciality-box">
                    <div class="speciality-img"><a href="#"><img src="{{asset('web-images/speciality-2.jpg')}}" alt="speciality" class="spec-image"></a></div>
                    <a href="#" class="ser-title text-uppercase font-weight-bold">Double Cheese </a>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 text-center speciality-box">
                    <div class="speciality-img"><a href="#"><img src="{{asset('web-images/speciality-3.jpg')}}" alt="speciality" class="spec-image"></a></div>
                    <a href="#" class="ser-title text-uppercase font-weight-bold">Love Love</a>
                </div>
            </div>
        </div>
    </section>

    <section class="special-menu ptb pt-140 special-menu-with-icons">
        <img  src="{{asset('web-images/lion-king.png')}}" alt="banner" class="top-left-icon">
        <img  src="{{asset('web-images/timon-pumbaa.png')}}" alt="banner" class="bottom-right-icon">
        <div class="container">
            <div class="menu-top-bg"><img src="{{asset('web-images/menu-top-bg.png')}}" alt="meu-bg"></div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="headding-part text-center pb-50">
                        <p  class="headding-sub">Best of Hakuna Matata</p>
                        <h2 class="headding-title text-uppercase font-weight-bold">Menuja jone speciale</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="special-tab text-center">
                        <ul  id="tabs" class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="text-uppercase font-weight-bold tab-link current" data-tab="tab-1"><a href="#tab-1" role="tab" data-toggle="tab" class="active"> Te gjitha</a></li>
                            <li role="presentation" class="text-uppercase font-weight-bold tab-link" data-tab="tab-2"><a href="#tab-2" role="tab" data-toggle="tab"> Pijet</a></li>
                            <li role="presentation" class="text-uppercase font-weight-bold tab-link" data-tab="tab-3"><a href="#tab-3" role="tab" data-toggle="tab"> Sallatat</a></li>
                            <li role="presentation" class="text-uppercase font-weight-bold tab-link" data-tab="tab-4"><a href="#tab-4" role="tab" data-toggle="tab"> pizza</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div role="tabpanel" class="row pt-50 tab-pane fade in active show" id="tab-1">
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/menu-1.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Wrustel</a>
                        <p class="menu-des">Salce,moxarela,wudy. Normale & Familjare </p>
                        <span class="menu-price">450/800L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/menu-3.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Pepsi/Cola</a>
                        <p class="menu-des">Te ftohta dhe ambjenti. </p>
                        <span class="menu-price">150L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/menu-7.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Sallate Hakuna Matata</a>
                        <p class="menu-des">Sallate e perfeksionuar nga Hakuna Matata. </p>
                        <span class="menu-price">400L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/fanta.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Fanta</a>
                        <p class="menu-des">Portokall, Exotic etj. </p>
                        <span class="menu-price">150 L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/heineken8.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Heineken</a>
                        <p class="menu-des">Shishe, Kanace dhe kriko. </p>
                        <span class="menu-price">250L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img  src="{{asset('web-images/menu-6.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Kapricoze</a>
                        <p class="menu-des">Salce,Moxarella,proshute,kerpurdhe,speca,ullinj.
                        Normale & Familjare</p>
                        <span class="menu-price">550/900L</span>
                    </div>
                </div>
                <div role="tabpanel" class="row pt-70 tab-pane fade" id="tab-2">

                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/menu-3.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Pepsi/Cola</a>
                        <p class="menu-des">Te ftohta dhe ambjenti. </p>
                        <span class="menu-price">150L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/fanta.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Fanta</a>
                        <p class="menu-des">Portokall, Exotic etj. </p>
                        <span class="menu-price">150 L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/heineken8.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Heineken</a>
                        <p class="menu-des">Shishe, Kanace dhe kriko. </p>
                        <span class="menu-price">250L</span>
                    </div>
                </div>

                <div role="tabpanel" class="row pt-70 tab-pane fade" id="tab-3">
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/menu-7.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Sallate Hakuna Matata</a>
                        <p class="menu-des">Sallate e perfeksionuar nga Hakuna Matata. </p>
                        <span class="menu-price">400L</span>
                    </div>
                </div>
                <div role="tabpanel" class="row pt-70 tab-pane fade" id="tab-4">

                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img src="{{asset('web-images/menu-1.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Wrustel</a>
                        <p class="menu-des">Salce,moxarela,wudy. Normale & Familjare </p>
                        <span class="menu-price">450/800L</span>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 text-center pt-20">
                        <div class="menu-img"><a href="#"><img  src="{{asset('web-images/menu-6.png')}}" alt="menu" class="menu-image"></a></div>
                        <a href="#" class="menu-title text-uppercase">Kapricoze</a>
                        <p class="menu-des">Salce,Moxarella,proshute,kerpurdhe,speca,ullinj.
                            Normale & Familjare</p>
                        <span class="menu-price">550/900L</span>
                    </div>

                </div>
            </div>
            <div class="menu-bottom-bg"><img src="{{asset('web-images/menu-bottom-bg.png')}}" alt="menu-bg"></div>
        </div>
    </section>



    <section class="menu-list pt-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="menu-tabbing">
                        <ul id="tabs" class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="tab-link current" data-tab="tab-100"><a href="#tab-100" role="tab" data-toggle="tab" class="active">Pizzat</a></li>
                            <li role="presentation" class="tab-link" data-tab="tab-200"><a href="#tab-200"  role="tab" data-toggle="tab">Sandwich</a></li>
                            <li role="presentation" class="tab-link" data-tab="tab-300"><a href="#tab-300"  role="tab" data-toggle="tab">Kalcone</a></li>
                            <li role="presentation" class="tab-link" data-tab="tab-400"><a href="#tab-400"  role="tab" data-toggle="tab">Sallata</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content">

                <div role="tabpanel" class="row tab-pane fade in active show" id="tab-100">
                    @foreach($menuItems as $menuItem)
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="menu-list-box-2">
                                {{--                            <div class="list-img-2">--}}
                                {{--                                <a href="shop-detail.html"><img src="images/list-12.jpg" alt="pizza"></a>--}}
                                {{--                            </div>--}}
                                <div class="menu-detail-2">
                                    <div class="iteam-name-list">
                                        <a href="#" class="iteam-name">{{$menuItem['name'] ? : ""}}</a>
                                        <span class="iteam-srice">{{$menuItem['price_1']  ? : ""}} L /  {{$menuItem['price_2']}} L</span>
                                    </div>
                                    <p class="iteam-desc">{{$menuItem['ingredients']  ? :  ""}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div role="tabpanel" class="row tab-pane fade" id="tab-200">

                    @foreach($sandwiches as $sandwich)
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="menu-list-box-2">

                            <div class="menu-detail-2">
                                <div class="iteam-name-list">
                                    <a href="#" class="iteam-name">{{$sandwich['name']}}</a>
                                    <span class="iteam-srice">{{$sandwich['price']}} Leke</span>
                                </div>
                                <p class="iteam-desc"></p>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div role="tabpanel" class="row tab-pane fade" id="tab-300">
                    @foreach($kalcones as $menuItem)
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="menu-list-box-2">
                                {{--                            <div class="list-img-2">--}}
                                {{--                                <a href="shop-detail.html"><img src="images/list-12.jpg" alt="pizza"></a>--}}
                                {{--                            </div>--}}
                                <div class="menu-detail-2">
                                    <div class="iteam-name-list">
                                        <a href="#" class="iteam-name">{{$menuItem['name'] ? : ""}}</a>
                                        <span class="iteam-srice">{{$menuItem['price']  ? : ""}} Leke</span>
                                    </div>
                                    <p class="iteam-desc">{{$menuItem['ingredients']  ? :  ""}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div role="tabpanel" class="row tab-pane fade" id="tab-400">
                    @foreach($salads as $salad)
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="menu-list-box-2">

                                <div class="menu-detail-2">
                                    <div class="iteam-name-list">
                                        <a href="#" class="iteam-name">{{$salad['name']}}</a>
                                        <span class="iteam-srice">{{$salad['price']}} Leke</span>
                                    </div>
                                    <p class="iteam-desc"></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="online-booking ptb">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="max-w-390">
                        <div class="headding-part">
                            <p  class="headding-sub">Hakuna Matata</p>
                            <h2 class="headding-title text-uppercase font-weight-bold">Porosit Tani</h2>
                        </div>
                        <p class="online-des">
                            Na telefononi , beni porosine tuaj dhe ne mund ta sjellim ate prane jush ne nje kohe rekord.
                        </p>
                        <a href="tel:+355 69 20 92 248" class="online-call">+355 69 20 92 248</a>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 text-center">
                    <h2 class="book-table text-uppercase">Rezervoni</h2>
                    <form class="online-order-form">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Emri" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" placeholder="Mbiemri" required>
                        </div>
                        <div class="form-group">
                            <select name="sources" id="sources" class="custom-select sources form-control" data-placeholder="Sa persona?">
                                <option value="profile" selected>5 persona</option>
                                <option value="word">4 persona</option>
                                <option value="hashtag">3 persona</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="date" placeholder="Data" required>
                        </div>
                        <button type="submit" class="more-table-v">rezervo tani</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="customer ptb">
        <div class="container">
            <div class="customer-inner">
                <div class="customer-top-bg"><img src="{{asset('images/customer-top-bg.png')}}" alt="customer"></div>
                <div class="headding-part pb-50 text-center">
                    <p  class="headding-sub">Cfare thone klientet tane?</p>
                </div>
                <div class="customer-slide owl-carousel">
                    <div class="customer-detail">
                        <div class="customer-reviews">
                            <p class="review-cus">Pizzat ishin tejet te shijshme dhe sherbimi i jashtezakonshem . Padyshim nje nga vendet me mikpritese qe kam qene.  </p>
                            <label class="post-name">Carlo Razzoli - <span>Gazetar</span></label>
                        </div>
                    </div>
                    <div class="customer-detail">

                        <div class="customer-reviews">
                            <p class="review-cus">Une dhe miqte e mi kaluam nje kohe shume te mire.
                                Gjithcka shkoi perfekt. Ushqimi , sherbimi , ambienti ishin te gjitha ne lartesine e duhur. </p>
                            <label class="post-name">Kevin Doda - <span>Ekonomist</span></label>
                        </div>
                    </div>
                    <div class="customer-detail">
                        <div class="customer-reviews">
                            <p class="review-cus">Do tua rekomandoja te gjithve qe vizitojne Tiranen . Bejne picat me te mira ne qytet. Sa here rikthehem ne Tirane me gjen aty .  </p>
                            <label class="post-name">Manfred Muller - <span>Biznesmen</span></label>
                        </div>
                    </div>
                </div>
                <div class="customer-bottom-bg"><img src="images/customer-bottom-bg.png" alt="customer"></div>
            </div>
        </div>
    </section>

    <section class="about-pizzon ptb about-pizzon-with-icons">
        <img src="{{asset('web-images/lion-king.png')}}" alt="banner" class="bottom-left-icon">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="max-w-390">
                        <div class="headding-part">
                            <p class="headding-sub">Hakuna Matata</p>
                            <h2 class="headding-title text-uppercase font-weight-bold">Rreth Nesh</h2>
                        </div>
                        <p class="online-des">Hakuna-Matata eshte hapur ne vitin 2021. Edhe pse nje nga pizzerite me te reja ne vend , eksperienca jone e meparshme dhe
                            sherbimi yne cilesor pati nje ndikim tejet te larte ne rritjen tone.Tashme ne ofrojme sherbimin home delivery. Mund te rezervoni nje tavoline nepermjet webit dhe duke na telefonuar .. </p>
{{--                        <a href="{{route('web.about-us')}}" class="about-more-z com-btn">Me shume</a>--}}
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="about-pizzon-img">
                        <img src="{{'web-images/about-pizzon.png'}}" alt="about" class="pizzon-ab">
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
