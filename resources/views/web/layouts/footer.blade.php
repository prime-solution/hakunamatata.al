<footer>
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 footer-box">
                    <div class="footer-logo">
                        <img src="{{asset('web-images/new-logo2.png')}}" alt="fooret-logo">
                        <p class="footer-des">Rruga Robert Shvarc prane shkolles "At Zef Pllumi", Tirane ,1046</p>
                        <ul>
                            <li>Telefon  - <a href="tel:+355 69 20 92 248">+355 69 20 92 248</a></li>
                            <li>email  - <a href="hakunamatatapizzeria@outlook.com">hakunamatatapizzeria@outlook.com</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 footer-box">
                    <div class="opening-hours">
                        <h2>Oret e punes</h2>
                        <ul>
                            <li>Hene - Diele :  <span>9.00 am - 11.00 pm</span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 footer-box">
                    <div class="useful-links">
                        <h2>links</h2>
                        <ul>
                            <li><a href="{{route('web.about-us')}}">Rreth nesh</a></li>
                            <li><a href="{{route('web.contact-us')}}">Na kontaktoni</a></li>
                            <li><a href="{{route('web.menu-list')}}">Menu</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 copyright-box">
                    <p class="copy-text">© Hakuna-Matata all Rights Reserved. Designed by <a href="#">Prime Solutions</a></p>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 copyright-box">
                    <ul>
                        <li><a href="https://www.facebook.com/Pizzeria-Hakuna-Matata-107875438333358"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.instagram.com/pizzeriahakunamatata/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
