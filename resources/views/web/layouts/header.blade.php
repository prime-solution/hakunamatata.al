<header id="header">
    <div class="container">
        <div class="row m-0">
            <div class="col-xl-3 col-lg-2 col-md-4 col-3 p-0">
                <div class="navbar-header">
                    <a class="navbar-brand page-scroll" href="{{route('web.index')}}">
                        <img alt="pizzon" src="{{asset('web-images/new-logo2.png')}}">
                    </a>
                </div>
            </div>
            <div class="col-xl-9 col-lg-10 col-md-8 col-9 p-0 text-right">
                <div id="menu" class="navbar-collapse collapse" >
                    <ul class="nav navbar-nav">
                        <li class="level">
                            <a href="{{route('web.index')}}" class="page-scroll">Kryefaqja</a>
                        </li>

                    </ul>
                </div>
                <div class=" header-right-link">
                    <ul>
                        <li class="call-icon">
                            <a href="tel:+355 69 20 92 248">
                                <span class="icon"></span>
                                <div class="link-text">+355 69 20 92 248</div>
                            </a>
                        </li>

                        <li class="side-toggle">
                            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"><span></span></button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
