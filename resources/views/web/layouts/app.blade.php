<!DOCTYPE html>
<html lang="en">

@include('web.layouts.head')


<body>

<!-- Start preloader -->
<div id="preloader">
    <label>Loading</label>
</div>
<!-- End preloader -->

@include('web.layouts.header')

@yield('content')

<div class="top-scrolling">
    <a href="#header" class="scrollTo"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
</div>

@include('web.layouts.footer')
@include('web.layouts.scripts')


</body>
</html>
